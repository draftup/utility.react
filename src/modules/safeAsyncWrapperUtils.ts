import * as React from "react";

type SafeAsyncWrapper = {
  wrap(promise: Promise<void>): void;
};

export function useSafeAsyncWrapper(): SafeAsyncWrapper {
  const [error, setError] = React.useState<null | Error>(null);

  if (error !== null) {
    throw error;
  }

  const wrap = React.useCallback((promise) => {
    promise.catch(setError);
  }, []);

  return { wrap };
}
