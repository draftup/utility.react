import * as React from "react";

import { AsyncProcess } from "@draftup/utility.js";
import { useSafeAsyncWrapper } from "./safeAsyncWrapperUtils";

type AsyncProcessWrapper<ValueType> = {
  process: AsyncProcess<ValueType>;
  wrap(promise: Promise<ValueType | Error>): void;
};

export function useAsyncProcessWrapper<
  ValueType
>(): AsyncProcessWrapper<ValueType> {
  const [process, setProcess] = React.useState<AsyncProcess<ValueType>>(
    new AsyncProcess()
  );

  const { wrap: wrapAsyncSafely } = useSafeAsyncWrapper();

  const counterRef = React.useRef(0);

  const wrap = React.useCallback(
    (promise: Promise<ValueType | Error>) => {
      counterRef.current++;

      const { current: currentCount } = counterRef;

      setProcess(new AsyncProcess<ValueType>().pend());

      wrapAsyncSafely(
        promise.then((value) => {
          if (currentCount === counterRef.current) {
            if (value instanceof Error) {
              setProcess((prevProcess) => {
                return new AsyncProcess(prevProcess).reject(value);
              });
            } else {
              setProcess((prevProcess) => {
                return new AsyncProcess(prevProcess).resolve(value);
              });
            }
          }
        })
      );
    },
    [wrapAsyncSafely]
  );

  return { process, wrap };
}
