## [1.1.1](https://gitlab.com/draftup/utility.react/compare/v1.1.0...v1.1.1) (2023-04-24)


### Bug Fixes

* export async process wrapper utils ([0efe582](https://gitlab.com/draftup/utility.react/commit/0efe58210eab188fd72619c26b67addd8a9870ab))

# [1.1.0](https://gitlab.com/draftup/utility.react/compare/v1.0.1...v1.1.0) (2023-04-24)


### Features

* async process wrapper ([444b512](https://gitlab.com/draftup/utility.react/commit/444b5123399fe9811303ca46c13a609056dcbd16))

## [1.0.1](https://gitlab.com/draftup/utility.react/compare/v1.0.0...v1.0.1) (2023-04-22)


### Bug Fixes

* support react versions greater than 16.0.0 ([edfe918](https://gitlab.com/draftup/utility.react/commit/edfe9187b52bd872f7cf43ad6b4c6aba528096e5))

# 1.0.0 (2023-04-22)


### Features

* safe async wrapper ([528f82d](https://gitlab.com/draftup/utility.react/commit/528f82d9680c5f7a5e27768a37f102b8d564fb7e))
